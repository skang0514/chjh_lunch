# from . import models
from django.shortcuts import render
from django.http import HttpResponse
import codecs


def uploadFile(request):
    if request.method == "POST":
        # Fetching the form data
        uploadedFile = request.FILES["uploadedFile"]
        import pandas as pd

        df = pd.read_excel(uploadedFile.file, sheet_name='詳細資料', usecols="C:H", engine='openpyxl')
        df = df.astype({'日期': 'int64', '班級': 'int64', '座號': 'int64'}, errors='ignore')
        _df = df.groupby(["班級", "座號", "請假學生"]).size().reset_index(name="次數")
        _df = _df.assign(金額=lambda x: x.次數 * 45)
        _df = _df.astype({'班級': 'int64', '座號': 'int64'})
        _df = _df.sort_values(["班級", "座號"], ascending=True)
        leave_list = []
        for index, row in _df.iterrows():
            student_leave = df[(df['請假學生'] == row['請假學生']) & (df['座號'] == row['座號']) & (df['班級'] == row['班級'])]
            student_leave_df = student_leave.groupby(["假別"]).size().reset_index(name="次數")
            _str = ''
            for idx, _row in student_leave_df.iterrows():
                if len(student_leave_df.index) == idx + 1:
                    _str += f'{_row["假別"]}({_row["次數"]})'
                else:
                    _str += f'{_row["假別"]}({_row["次數"]}),'
            leave_list.append(_str)
        _df.insert(loc=len(_df.columns), column='假別', value=leave_list)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=export.csv'  # alter as needed
        response.write(codecs.BOM_UTF8)

        _df.to_csv(path_or_buf=response)  # with other applicable parameters
        return response
    return render(request, 'Core/upload-file.html')